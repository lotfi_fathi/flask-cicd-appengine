import os
import logging
from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello World from Gitlab'

@app.route('/calc')
def calc():
    a = request.args.get('a')
    b = request.args.get('b')
    return str(sum(int(a), int(b)))

@app.errorhandler(500)
def server_error(e):
    logging.exception("An error occurred during a request.")
    return (
        """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(
            e
        ),
        500,
    )

def sum(a, b):
    return a + b


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)
